$(function () {

    $('#btnHide').on('click', function () {
        hideDivs();
    })

    $('#btnShow').on('click', function () {
        showDivs();
    })

    $('#btnToggle1').on('click', function () {
      $('#div1').toggle();
    })

})

function hideDivs() {
    $('#div1').hide();
    $('#div2').hide();
}

function showDivs() {

    $('#div1').show();
    $('#div2').show();
}
