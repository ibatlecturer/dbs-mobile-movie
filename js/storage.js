//Useful reference:
//https://www.smashingmagazine.com/2010/10/local-storage-and-how-to-use-it/
$(function () {

    init(); // handy way to initialise all your functions in one place

})
function init() {

    $('#btnMakeBooking').on('click', function () {

        var bookingDetails = {email: '', age: '', fullName: ''};


        bookingDetails.email = $('#tbEmail').val();
        bookingDetails.age =$('#tbAge').val();
        bookingDetails.fullName = $('#tbFullName').val();
        console.log(bookingDetails);
        //console.log(`Making a Booking for ${bookingEmail}`);

       saveBooking(bookingDetails);

    })

    $('#btnRetrieveBooking').on('click', function () {
        retrieveBooking();
    })

}

function saveBooking(bookingDetails) {
  //  console.log(`Booking for ${booking}`);


    localStorage.setItem('bookingDetails', JSON.stringify(bookingDetails));


    $('#divMakeBooking').hide();
    $('#divRetrieveBooking').show();
}

function retrieveBooking() {
    var bookingDetails = JSON.parse(localStorage.getItem('bookingDetails'));
    console.log(`Booking retrieved for ${bookingDetails.email}`);
    $('#bookingEmail').html(bookingDetails.email);
    $('#bookingAge').html(bookingDetails.age);
    $('#bookingFullName').html(bookingDetails.fullName);

}